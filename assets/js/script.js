$(document).ready(function(){
	var $jours = $('.jour');
	var $puces = $('.bullets .entypo-record	');
	function init(){
		setTimeout(function(){
			$('body').addClass('isok');
			$jours.hide();
			$('.wrapper').fadeIn('slow', function(){
				$jours.first().fadeIn('slow');
				/*	Il manquait des () à la fonction first si dessous.	*/
				$puces.removeClass('active').first().addClass('active');
			});
		}, 2000);	
	};
	/*Il manquait un s à $puce si dessous.*/
	$puces.click(function(){
		var $this = $(this);
		var cible = $this.attr('data-cible');
		$jours.hide();
		/*Il manquait un ; après fadeIn() si dessous.*/
		$($jours.get(cible)).fadeIn();
		$puces.removeClass('active');
		$this.addClass('active');
	});
	init();
});
/*Il manquait } avant la ) si dessus.*/  